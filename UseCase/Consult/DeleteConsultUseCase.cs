using Domain.Consult.Adapters;
using Domain.Consult.Ports;

namespace UseCase.Consult
{
    public class DeleteConsultUseCase : IDeleteConsultPort
    {
        private readonly IDeleteConsultAdapter deleteConsultAdapter;

        public DeleteConsultUseCase(IDeleteConsultAdapter deleteConsultAdapter)
        {
            this.deleteConsultAdapter = deleteConsultAdapter;
        }

        public void DeleteConsult(string code)
        {
            this.deleteConsultAdapter.DeleteConsult(code);
        }
    }
}
