using Domain.Consult.Adapters;
using Domain.Consult.Exceptions;
using Domain.Consult.Ports;
using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;

namespace UseCase.Consult
{
    public class CreateConsultUseCase : ICreateConsultPort
    {
        private readonly IFindConsultAdapter findConsultAdapter;
        private readonly ICreateConsultAdapter createConsultAdapter;

        private readonly IFindStandardAdapter findStandardAdapter;

        public CreateConsultUseCase(IFindConsultAdapter findConsultAdapter, ICreateConsultAdapter createConsultAdapter, IFindStandardAdapter findStandardAdapter)
        {
            this.findConsultAdapter = findConsultAdapter;
            this.createConsultAdapter = createConsultAdapter;
            this.findStandardAdapter = findStandardAdapter;
        }

        public void CreateConsult(Domain.Consult.Models.Consult consult)
        {
            var foundConsult = this.findConsultAdapter.FindConsultByCode(consult.Code);
            if (foundConsult != null)
            {
                throw new ConsultAlreadyExistsException(foundConsult);
            }

            consult.Standard = this.findStandardAdapter.FindStandardByCode(consult.StandardCode);

            this.createConsultAdapter.CreateConsult(consult);
        }
    }
}
