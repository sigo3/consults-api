using System.Linq;
using Domain.Common;
using Domain.Consult.Adapters;
using Domain.Consult.Ports;
using Domain.Standard.Adapters;

namespace UseCase.Consult
{
    public class ListConsultUseCase : IListConsultPort
    {
        private readonly IListConsultAdapter listConsultAdapter;
        private readonly IFindStandardAdapter findStandardAdapter;

        public ListConsultUseCase(IListConsultAdapter listConsultAdapter, IFindStandardAdapter findStandardAdapter)
        {
            this.listConsultAdapter = listConsultAdapter;
            this.findStandardAdapter = findStandardAdapter;
        }

        public PaginatedResult<Domain.Consult.Models.Consult> ListConsult(int page, int pageSize)
        {
            var result = this.listConsultAdapter.ListConsult(page, pageSize);

            var items = result.Items
                .Select(item =>
                {
                    item.Standard = findStandardAdapter.FindStandardByCode(item.StandardCode);
                    return item;
                });

            result.Items = items;

            return result;
        }
    }
}
