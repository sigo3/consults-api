using Domain.Consult.Adapters;
using Domain.Consult.Ports;

namespace UseCase.Consult
{
    public class UpdateConsultUseCase : IUpdateConsultPort
    {
        private readonly IUpdateConsultAdapter updateConsultAdapter;

        public UpdateConsultUseCase(IUpdateConsultAdapter updateConsultAdapter)
        {
            this.updateConsultAdapter = updateConsultAdapter;
        }

        public void UpdateConsult(string code, Domain.Consult.Models.Consult consult)
        {
            this.updateConsultAdapter.UpdateConsult(code, consult);
        }
    }
}
