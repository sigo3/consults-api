using Domain.Consult.Adapters;
using Domain.Consult.Exceptions;
using Domain.Consult.Ports;
using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;

namespace UseCase.Consult
{
    public class FindConsultUseCase : IFindConsultByCodePort
    {
        private readonly IFindConsultAdapter findConsultAdapter;
        private readonly IFindStandardAdapter findStandardAdapter;

        public FindConsultUseCase(IFindConsultAdapter findConsultAdapter, IFindStandardAdapter findStandardAdapter)
        {
            this.findConsultAdapter = findConsultAdapter;
            this.findStandardAdapter = findStandardAdapter;
        }

        public Domain.Consult.Models.Consult FindConsultByCode(string code)
        {
            var consult = this.findConsultAdapter.FindConsultByCode(code);
            if (consult == null)
            {
                throw new ConsultNotFoundException(code);
            }

            consult.Standard = findStandardAdapter.FindStandardByCode(consult.StandardCode);

            return consult;
        }
    }
}
