using Domain.Standard.Adapters;
using Domain.Standard.Exceptions;
using Domain.Standard.Ports;

namespace UseCase.Standard
{
    public class FindStandardUseCase : IFindStandardByCodePort
    {
        private readonly IFindStandardAdapter findStandardAdapter;

        public FindStandardUseCase(IFindStandardAdapter findStandardAdapter)
        {
            this.findStandardAdapter = findStandardAdapter;
        }

        public Domain.Standard.Models.Standard FindStandardByCode(string code)
        {
            var standard = this.findStandardAdapter.FindStandardByCode(code);
            if (standard == null) {
                throw new StandardNotFoundException(code);
            }

            return standard;
        }
    }
}
