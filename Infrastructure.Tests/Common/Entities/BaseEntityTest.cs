using FizzWare.NBuilder;

using FluentAssertions;

using Infrastructure.Common.Entities;

using Xunit;

namespace Infrastructure.Tests.Common.Entities
{
    public class BaseEntityTest
    {
        private readonly BaseEntity baseEntity;

        public BaseEntityTest()
        {
            this.baseEntity = Builder<Infrastructure.Consult.Entities.Consult>.CreateNew().Build();
        }

        [Fact]
        public void TestCopyValues()
        {
            var entity = Builder<Infrastructure.Consult.Entities.Consult>.CreateNew().Build();
            var model = new Domain.Consult.Models.Consult();

            BaseEntity.Copy<Infrastructure.Consult.Entities.Consult, Domain.Consult.Models.Consult>(entity, model);

            model.Code.Should().Be(entity.Code);
            model.Company.Should().Be(entity.Company);
            model.Description.Should().Be(entity.Description);
            model.Status.Should().Be(entity.Status);
        }
    }
}
