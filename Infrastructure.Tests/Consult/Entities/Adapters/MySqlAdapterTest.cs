using Arch.EntityFrameworkCore.UnitOfWork;

using AutoMapper;

using FizzWare.NBuilder;

using Infrastructure.Common.Contexts;
using Infrastructure.Consult.Entities.Adapters;

using Moq;

using Xunit;

namespace Infrastructure.Tests.Consult.Entities.Adapters
{
    public class MySqlAdapterTest
    {
        private readonly Mock<IUnitOfWork<MySqlContext>> unitOfWorkMock;
        private readonly Mock<IRepository<Infrastructure.Consult.Entities.Consult>> consultRepositoryMock;
        private readonly Mock<IMapper> mapperMock;

        private readonly MySqlAdapter mySqlAdapter;

        public MySqlAdapterTest()
        {
            this.unitOfWorkMock = new Mock<IUnitOfWork<MySqlContext>>();
            this.consultRepositoryMock = new Mock<IRepository<Infrastructure.Consult.Entities.Consult>>();
            this.mapperMock = new Mock<IMapper>();

            this.unitOfWorkMock.Setup(
                mock => mock.GetRepository<Infrastructure.Consult.Entities.Consult>(false)
            ).Returns(this.consultRepositoryMock.Object);

            this.mySqlAdapter = new MySqlAdapter(
                this.unitOfWorkMock.Object,
                this.mapperMock.Object
            );
        }

        [Fact]
        public void TestCreateConsultWithSuccess()
        {
            var entity = Builder<Infrastructure.Consult.Entities.Consult>.CreateNew().Build();
            var model = new Domain.Consult.Models.Consult();

            Infrastructure.Consult.Entities.Consult.Copy<Infrastructure.Consult.Entities.Consult, Domain.Consult.Models.Consult>(entity, model);

            mySqlAdapter.CreateConsult(model);

            consultRepositoryMock.Verify(
                mock => mock.Insert(It.IsAny<Infrastructure.Consult.Entities.Consult>()), Times.Once()
            );

            unitOfWorkMock.Verify(
                mock => mock.SaveChanges(false), Times.Once()
            );
        }

        [Fact]
        public void TestDeleteConsultWithSuccess()
        {
            var consult = Builder<Infrastructure.Consult.Entities.Consult>.CreateNew().Build();

            mySqlAdapter.DeleteConsult(consult.Code);

            consultRepositoryMock.Verify(
                mock => mock.Delete(It.IsAny<Infrastructure.Consult.Entities.Consult>()), Times.Once()
            );

            unitOfWorkMock.Verify(
                mock => mock.SaveChanges(false), Times.Once()
            );
        }

        [Fact(Skip="Out of scope")]
        public void TestFindConsultByCodeConsultWithSuccess()
        {
            var consult = Builder<Infrastructure.Consult.Entities.Consult>.CreateNew().Build();

            mySqlAdapter.FindConsultByCode(consult.Code);

            consultRepositoryMock.Verify(
                mock => mock.GetFirstOrDefault(
                    _consult => _consult.Code == consult.Code,
                    null,
                    null,
                    true,
                    false
                ), Times.Once()
            );
        }

        [Fact(Skip="Out of scope")]
        public void TestListConsultWithSuccess()
        {
            mySqlAdapter.ListConsult(0, 20);

            consultRepositoryMock.Verify(
                mock => mock.GetPagedList<Infrastructure.Consult.Entities.Consult>(
                    _consult => _consult,
                    null,
                    null,
                    null,
                    0,
                    20,
                    true,
                    false
                ), Times.Once()
            );
        }

        [Fact(Skip="Out of scope")]
        public void TestUpdateConsultWithSuccess()
        {
            var entity = Builder<Infrastructure.Consult.Entities.Consult>.CreateNew().Build();
            var model = new Domain.Consult.Models.Consult();

            Infrastructure.Consult.Entities.Consult.Copy<Infrastructure.Consult.Entities.Consult, Domain.Consult.Models.Consult>(entity, model);

            consultRepositoryMock.Setup(
                mock => mock.GetFirstOrDefault(
                    _consult => _consult.Code == entity.Code,
                    null,
                    null,
                    true,
                    false
                )
            ).Returns(entity);

            mySqlAdapter.UpdateConsult(model.Code, model);

            consultRepositoryMock.Verify(
                mock => mock.Update(It.IsAny<Infrastructure.Consult.Entities.Consult>()), Times.Once()
            );

            unitOfWorkMock.Verify(
                mock => mock.SaveChanges(false), Times.Once()
            );
        }
    }
}
