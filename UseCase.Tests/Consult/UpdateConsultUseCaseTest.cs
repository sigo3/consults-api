using Domain.Consult.Adapters;
using Domain.Consult.Exceptions;

using FizzWare.NBuilder;

using Moq;

using UseCase.Consult;

using Xunit;

namespace UseCase.Tests.Consult
{
    public class UpdateConsultUseCaseTest
    {
        private readonly Mock<IUpdateConsultAdapter> updateConsultAdapterMock;

        private readonly UpdateConsultUseCase updateConsultUseCase;

        public UpdateConsultUseCaseTest()
        {
            this.updateConsultAdapterMock = new Mock<IUpdateConsultAdapter>();

            this.updateConsultUseCase = new UpdateConsultUseCase(
                this.updateConsultAdapterMock.Object
            );
        }

        [Fact]
        public void TestUpdateConsultWithSuccess()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            updateConsultUseCase.UpdateConsult(consult.Code, consult);

            updateConsultAdapterMock.Verify(
                mock => mock.UpdateConsult(It.IsAny<string>(), It.IsAny<Domain.Consult.Models.Consult>()), Times.Once()
            );
        }

        [Fact]
        public void TestUpdateConsultNotFound()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            updateConsultAdapterMock.Setup(
                mock => mock.UpdateConsult(consult.Code, It.IsAny<Domain.Consult.Models.Consult>())
            ).Throws(new ConsultNotFoundException(consult.Code));

            Assert.Throws<ConsultNotFoundException>(() =>
            {
                updateConsultUseCase.UpdateConsult(consult.Code, consult);
            });
        }
    }
}
