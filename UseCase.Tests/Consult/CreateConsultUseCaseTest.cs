using Domain.Consult.Adapters;
using Domain.Consult.Exceptions;
using Domain.Standard.Adapters;
using FizzWare.NBuilder;

using Moq;

using UseCase.Consult;

using Xunit;

namespace UseCase.Tests.Consult
{
    public class CreateConsultUseCaseTest
    {
        private readonly Mock<IFindConsultAdapter> findConsultAdapterMock;
        private readonly Mock<ICreateConsultAdapter> createConsultAdapterMock;
        private readonly Mock<IFindStandardAdapter> findStandardAdapterMock;

        private readonly CreateConsultUseCase createConsultUseCase;

        public CreateConsultUseCaseTest()
        {
            this.findConsultAdapterMock = new Mock<IFindConsultAdapter>();
            this.createConsultAdapterMock = new Mock<ICreateConsultAdapter>();
            this.findStandardAdapterMock = new Mock<IFindStandardAdapter>();

            this.createConsultUseCase = new CreateConsultUseCase(
                this.findConsultAdapterMock.Object,
                this.createConsultAdapterMock.Object,
                findStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestCreateConsultWithSuccess()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            createConsultUseCase.CreateConsult(consult);

            createConsultAdapterMock.Verify(
                mock => mock.CreateConsult(It.IsAny<Domain.Consult.Models.Consult>()), Times.Once()
            );
        }

        [Fact]
        public void TestCreateConsultAlreadyExists()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            findConsultAdapterMock.Setup(
                mock => mock.FindConsultByCode(It.IsAny<string>())
            ).Returns(consult);

            Assert.Throws<ConsultAlreadyExistsException>(() =>
            {
                createConsultUseCase.CreateConsult(consult);
            });
        }
    }
}
