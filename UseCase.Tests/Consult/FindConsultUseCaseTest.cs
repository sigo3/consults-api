using Domain.Consult.Adapters;
using Domain.Consult.Exceptions;
using Domain.Standard.Adapters;
using FizzWare.NBuilder;

using Moq;

using UseCase.Consult;

using Xunit;

namespace UseCase.Tests.Consult
{
    public class FindConsultUseCaseTest
    {
        private readonly Mock<IFindConsultAdapter> findConsultAdapterMock;
        private readonly Mock<IFindStandardAdapter> findStandardAdapterMock;

        private readonly FindConsultUseCase findConsultUseCase;

        public FindConsultUseCaseTest()
        {
            this.findConsultAdapterMock = new Mock<IFindConsultAdapter>();
            this.findStandardAdapterMock = new Mock<IFindStandardAdapter>();

            this.findConsultUseCase = new FindConsultUseCase(
                this.findConsultAdapterMock.Object,
                findStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestFindConsultWithSuccess()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            findConsultAdapterMock.Setup(
                mock => mock.FindConsultByCode(It.IsAny<string>())
            ).Returns(consult);

            findConsultUseCase.FindConsultByCode(consult.Code);

            findConsultAdapterMock.Verify(
                mock => mock.FindConsultByCode(It.IsAny<string>()), Times.Once()
            );
        }

        [Fact]
        public void TestFindConsultNotFound()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            Assert.Throws<ConsultNotFoundException>(() =>
            {
                findConsultUseCase.FindConsultByCode(consult.Code);
            });
        }
    }
}
