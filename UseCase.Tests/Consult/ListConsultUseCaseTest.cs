using Domain.Consult.Adapters;
using Domain.Standard.Adapters;
using FizzWare.NBuilder;

using Moq;

using UseCase.Consult;

using Xunit;

namespace UseCase.Tests.Consult
{
    public class ListConsultUseCaseTest
    {
        private readonly Mock<IListConsultAdapter> listConsultAdapterMock;
        private readonly Mock<IFindStandardAdapter> findStandardAdapterMock;

        private readonly ListConsultUseCase listConsultUseCase;

        public ListConsultUseCaseTest()
        {
            this.listConsultAdapterMock = new Mock<IListConsultAdapter>();
            this.findStandardAdapterMock = new Mock<IFindStandardAdapter>();

            this.listConsultUseCase = new ListConsultUseCase(
                this.listConsultAdapterMock.Object,
                this.findStandardAdapterMock.Object
            );
        }

        [Fact]
        public void TestListConsultWithSuccess()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            listConsultUseCase.ListConsult(It.IsAny<int>(), It.IsAny<int>());

            listConsultAdapterMock.Verify(
                mock => mock.ListConsult(It.IsAny<int>(), It.IsAny<int>()), Times.Once()
            );
        }
    }
}
