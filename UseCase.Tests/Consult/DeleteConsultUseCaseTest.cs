using Domain.Consult.Adapters;

using FizzWare.NBuilder;

using Moq;

using UseCase.Consult;

using Xunit;

namespace UseCase.Tests.Consult
{
    public class DeleteConsultUseCaseTest
    {
        private readonly Mock<IDeleteConsultAdapter> deleteConsultAdapterMock;

        private readonly DeleteConsultUseCase deleteConsultUseCase;

        public DeleteConsultUseCaseTest()
        {
            this.deleteConsultAdapterMock = new Mock<IDeleteConsultAdapter>();

            this.deleteConsultUseCase = new DeleteConsultUseCase(
                this.deleteConsultAdapterMock.Object
            );
        }

        [Fact]
        public void TestDeleteConsultWithSuccess()
        {
            var consult = Builder<Domain.Consult.Models.Consult>.CreateNew().Build();

            deleteConsultUseCase.DeleteConsult(consult.Code);

            deleteConsultAdapterMock.Verify(
                mock => mock.DeleteConsult(It.IsAny<string>()), Times.Once()
            );
        }
    }
}
