using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using Infrastructure.Common.Entities;

namespace Infrastructure.Common.Contexts
{
    public class MySqlContext : DbContext
    {
        public DbSet<Consult.Entities.Consult> Consults { get; set; }

        public MySqlContext(DbContextOptions<MySqlContext> options) : base(options)
        {
            this.Database.EnsureCreated();
        }

        public override int SaveChanges()
        {

            IEnumerable<EntityEntry> entries = ChangeTracker
                .Entries()
                .Where(entry => entry.State == EntityState.Added || entry.State == EntityState.Modified);

            var now = DateTime.UtcNow;
            foreach (var entry in entries)
            {
                var entity = entry.Entity as BaseEntity;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                }

                entity.UpdatedDate = now;
            }

            return base.SaveChanges();

        }
    }
}
