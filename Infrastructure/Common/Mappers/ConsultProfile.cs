using AutoMapper;

namespace Infrastructure.Common.Mappers
{
    public class ConsultProfile : Profile
    {
        public ConsultProfile()
        {
            CreateMap<Domain.Consult.Models.Consult, Infrastructure.Consult.Entities.Consult>().ReverseMap();
        }
    }
}
