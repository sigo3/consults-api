using System.Collections.Generic;

using Arch.EntityFrameworkCore.UnitOfWork;

using AutoMapper;

using Domain.Consult.Adapters;
using Models = Domain.Consult.Models;

using Infrastructure.Common.Contexts;
using Domain.Common;
using Domain.Consult.Exceptions;

namespace Infrastructure.Consult.Entities.Adapters
{
    public class MySqlAdapter :
        ICreateConsultAdapter,
        IDeleteConsultAdapter,
        IFindConsultAdapter,
        IListConsultAdapter,
        IUpdateConsultAdapter
    {
        private readonly IUnitOfWork<MySqlContext> unitOfWork;
        private readonly IMapper mapper;

        public MySqlAdapter(IUnitOfWork<MySqlContext> unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public void CreateConsult(Models.Consult consult)
        {
            unitOfWork.GetRepository<Entities.Consult>().Insert(
                mapper.Map<Models.Consult, Entities.Consult>(consult)
            );

            unitOfWork.SaveChanges();
        }

        public void DeleteConsult(string code)
        {
            var consult = unitOfWork.GetRepository<Entities.Consult>().GetFirstOrDefault(
                predicate: _consult => _consult.Code == code
            );

            unitOfWork.GetRepository<Consult>().Delete(consult);

            unitOfWork.SaveChanges();
        }

        public Models.Consult FindConsultByCode(string code)
        {
            var consult = unitOfWork.GetRepository<Entities.Consult>().GetFirstOrDefault(
                predicate: _consult => _consult.Code == code
            );

            return mapper.Map<Entities.Consult, Models.Consult>(consult);
        }

        public PaginatedResult<Models.Consult> ListConsult(int page, int pageSize)
        {
            var result = unitOfWork.GetRepository<Entities.Consult>()
                .GetPagedList<Entities.Consult>(
                    selector: _consult => _consult,
                    pageIndex: page,
                    pageSize: pageSize
                );

            return new PaginatedResult<Models.Consult>
            {
                Items = mapper.Map<IEnumerable<Entities.Consult>, IEnumerable<Models.Consult>>(result.Items),
                TotalCount = result.TotalCount,
                TotalPages = result.TotalPages,
                Page = result.PageIndex,
                PageSize = result.PageSize
            };
        }

        public void UpdateConsult(string code, Models.Consult consult)
        {
            var foundConsult = unitOfWork.GetRepository<Entities.Consult>().GetFirstOrDefault(
                predicate: _consult => _consult.Code == code
            );

            if (foundConsult == null)
            {
                throw new ConsultNotFoundException(consult.Code);
            }

            Entities.Consult.Copy<Models.Consult, Entities.Consult>(consult, foundConsult);

            unitOfWork.GetRepository<Entities.Consult>().Update(foundConsult);

            unitOfWork.SaveChanges();
        }
    }
}
