using System;

using Domain.Consult.Enums;

using Infrastructure.Common.Entities;

namespace Infrastructure.Consult.Entities
{
    public class Consult : BaseEntity
    {
        public string Code { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public ConsultStatus Status { get; set; }

        public string StandardCode { get; set; }
    }
}
