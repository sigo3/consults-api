using System.Threading.Tasks;
using Refit;

namespace Infrastructure.Standard.Adapters
{
    public interface IFindStandardRestAdapter
    {
        [Get("/{code}")]
        Task<Domain.Standard.Models.Standard> FindStandardByCodeAsync(string code);
    }
}
