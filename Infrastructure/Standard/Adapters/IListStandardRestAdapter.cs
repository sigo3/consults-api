using System.Threading.Tasks;
using Domain.Common;
using Refit;

namespace Infrastructure.Standard.Adapters
{
    public interface IListStandardRestAdapter
    {
        [Get("/?page={page}&pageSize={pageSize}")]
        Task<PaginatedResult<Domain.Standard.Models.Standard>> ListStandardAsync(int page, int pageSize);
    }
}
