using Domain.Common;
using Domain.Standard.Adapters;

namespace Infrastructure.Standard.Adapters
{
    public class RestAdapter : IFindStandardAdapter, IListStandardAdapter
    {
        private readonly IFindStandardRestAdapter findStandardRestAdapter;
        private readonly IListStandardRestAdapter listStandardRestAdapter;

        public RestAdapter(IFindStandardRestAdapter findStandardRestAdapter, IListStandardRestAdapter listStandardRestAdapter)
        {
            this.findStandardRestAdapter = findStandardRestAdapter;
            this.listStandardRestAdapter = listStandardRestAdapter;
        }

        public Domain.Standard.Models.Standard FindStandardByCode(string code)
        {
            return findStandardRestAdapter.FindStandardByCodeAsync(code).GetAwaiter().GetResult();
        }

        public PaginatedResult<Domain.Standard.Models.Standard> ListStandard(int page, int pageSize)
        {
            return listStandardRestAdapter.ListStandardAsync(page, pageSize).GetAwaiter().GetResult();
        }
    }
}
