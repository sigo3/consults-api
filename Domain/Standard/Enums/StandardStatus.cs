namespace Domain.Standard.Enums
{
    public enum StandardStatus
    {
        PendingApproval = 0,
        Outdated = 1,
        Operative = 2,
        NotOperative = 3
    }
}
