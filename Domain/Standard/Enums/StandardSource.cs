namespace Domain.Standard.Enums
{
    public enum StandardSource
    {
        Internal = 0,
        External = 1
    }
}
