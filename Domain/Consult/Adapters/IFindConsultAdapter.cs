namespace Domain.Consult.Adapters
{
    public interface IFindConsultAdapter
    {
        Consult.Models.Consult FindConsultByCode(string code);
    }
}
