namespace Domain.Consult.Adapters
{
    public interface IDeleteConsultAdapter
    {
        void DeleteConsult(string code);
    }
}
