namespace Domain.Consult.Adapters
{
    public interface ICreateConsultAdapter
    {
        void CreateConsult(Consult.Models.Consult consult);
    }
}
