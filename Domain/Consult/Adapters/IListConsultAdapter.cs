using Domain.Common;

namespace Domain.Consult.Adapters
{
    public interface IListConsultAdapter
    {
        PaginatedResult<Consult.Models.Consult> ListConsult(int page, int pageSize);
    }
}
