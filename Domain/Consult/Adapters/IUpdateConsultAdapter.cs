namespace Domain.Consult.Adapters
{
    public interface IUpdateConsultAdapter
    {
        void UpdateConsult(string code, Consult.Models.Consult consult);
    }
}
