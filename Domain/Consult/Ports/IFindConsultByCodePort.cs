namespace Domain.Consult.Ports
{
    public interface IFindConsultByCodePort
    {
        Consult.Models.Consult FindConsultByCode(string code);
    }
}
