namespace Domain.Consult.Ports
{
    public interface IDeleteConsultPort
    {
        void DeleteConsult(string code);
    }
}
