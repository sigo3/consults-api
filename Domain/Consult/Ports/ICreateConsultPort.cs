namespace Domain.Consult.Ports
{
    public interface ICreateConsultPort
    {
        void CreateConsult(Consult.Models.Consult consult);
    }
}
