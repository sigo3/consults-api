namespace Domain.Consult.Ports
{
    public interface IUpdateConsultPort
    {
        void UpdateConsult(string code, Consult.Models.Consult consult);
    }
}
