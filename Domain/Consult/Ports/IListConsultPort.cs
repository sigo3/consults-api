using Domain.Common;

namespace Domain.Consult.Ports
{
    public interface IListConsultPort
    {
        PaginatedResult<Consult.Models.Consult> ListConsult(int page, int pageSize);
    }
}
