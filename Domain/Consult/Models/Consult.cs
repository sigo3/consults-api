using Domain.Consult.Enums;

namespace Domain.Consult.Models
{
    public class Consult
    {
        public string Code { get; set; }

        public string Company { get; set; }

        public string Description { get; set; }

        public ConsultStatus Status { get; set; }

        public string StandardCode { get; set; }

        public Standard.Models.Standard Standard { get; set; }
    }
}
