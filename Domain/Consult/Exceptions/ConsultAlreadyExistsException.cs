using System;

namespace Domain.Consult.Exceptions
{
    public class ConsultAlreadyExistsException : Exception
    {
        public ConsultAlreadyExistsException(Consult.Models.Consult consult) : base($"Consult with code {consult.Code} already exists.") { }
    }
}
