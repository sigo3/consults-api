using System;

namespace Domain.Consult.Exceptions
{
    public class ConsultNotFoundException : Exception
    {
        public ConsultNotFoundException(string code) : base($"Consult with code {code} not found") { }
    }
}
