using System;
using System.Net;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Domain.Consult.Exceptions;
using Domain.Consult.Models;
using Domain.Consult.Ports;

using FizzWare.NBuilder;

using FluentAssertions;

using Moq;

using WebApi.Controllers;

using Xunit;
using Domain.Common;

namespace WebApi.Tests.Controllers
{
    public class ConsultControllerTest
    {

        private readonly Mock<ILogger<ConsultController>> loggerMock;
        private readonly Mock<ICreateConsultPort> createConsultPortMock;
        private readonly Mock<IDeleteConsultPort> deleteConsultPortMock;
        private readonly Mock<IFindConsultByCodePort> findConsultByCodePortMock;
        private readonly Mock<IListConsultPort> listConsultPortMock;
        private readonly Mock<IUpdateConsultPort> updateConsultPortMock;

        private readonly ConsultController consultController;

        public ConsultControllerTest()
        {
            this.loggerMock = new Mock<ILogger<ConsultController>>();
            this.createConsultPortMock = new Mock<ICreateConsultPort>();
            this.deleteConsultPortMock = new Mock<IDeleteConsultPort>();
            this.findConsultByCodePortMock = new Mock<IFindConsultByCodePort>();
            this.listConsultPortMock = new Mock<IListConsultPort>();
            this.updateConsultPortMock = new Mock<IUpdateConsultPort>();

            this.consultController = new ConsultController(
                this.loggerMock.Object,
                this.createConsultPortMock.Object,
                this.deleteConsultPortMock.Object,
                this.findConsultByCodePortMock.Object,
                this.listConsultPortMock.Object,
                this.updateConsultPortMock.Object
            );
        }

        [Fact]
        public void TestCreateConsultReturnCreated()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            var result = consultController.CreateConsult(consult) as CreatedResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.Created);
        }

        [Fact]
        public void TestCreateConsultReturnBadRequest()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            createConsultPortMock.Setup(
                mock => mock.CreateConsult(It.IsAny<Consult>())
            ).Throws(new ConsultAlreadyExistsException(consult));

            var result = consultController.CreateConsult(consult) as BadRequestObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.BadRequest);
        }

        [Fact]
        public void TestCreateConsultReturnInternalServerError()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            createConsultPortMock.Setup(
                mock => mock.CreateConsult(It.IsAny<Consult>())
            ).Throws<Exception>();

            var result = consultController.CreateConsult(consult) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void TestUpdateConsultReturnOk()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            var result = consultController.UpdateConsult(consult.Code, consult) as OkResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Fact]
        public void TestUpdateConsultReturnNotFound()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            updateConsultPortMock.Setup(
                mock => mock.UpdateConsult(It.IsAny<string>(), It.IsAny<Consult>())
            ).Throws(new ConsultNotFoundException(consult.Code));

            var result = consultController.UpdateConsult(consult.Code, consult) as NotFoundObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void TestUpdateConsultReturnInternalServerError()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            updateConsultPortMock.Setup(
                mock => mock.UpdateConsult(It.IsAny<string>(), It.IsAny<Consult>())
            ).Throws<Exception>();

            var result = consultController.UpdateConsult(consult.Code, consult) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void TestFindConsultByCodeReturnOk()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            findConsultByCodePortMock.Setup(
                mock => mock.FindConsultByCode(It.IsAny<string>())
            ).Returns(consult);

            var result = consultController.FindConsultByCode(consult.Code) as OkObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Fact]
        public void TestFindConsultByCodeReturnNotFound()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            findConsultByCodePortMock.Setup(
                mock => mock.FindConsultByCode(It.IsAny<string>())
            ).Throws(new ConsultNotFoundException(consult.Code));

            var result = consultController.FindConsultByCode(consult.Code) as NotFoundObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.NotFound);
        }

        [Fact]
        public void TestFindConsultByCodeReturnInternalServerError()
        {
            var consult = Builder<Consult>.CreateNew().Build();

            findConsultByCodePortMock.Setup(
                mock => mock.FindConsultByCode(It.IsAny<string>())
            ).Throws<Exception>();

            var result = consultController.FindConsultByCode(consult.Code) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }

        [Fact]
        public void TestListConsultReturnOk()
        {
            var consults = Builder<PaginatedResult<Consult>>.CreateNew().Build();

            listConsultPortMock.Setup(
                mock => mock.ListConsult(It.IsAny<int>(), It.IsAny<int>())
            ).Returns(consults);

            var result = consultController.ListConsult(0, 10) as OkObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.OK);
        }

        [Fact]
        public void TestListConsultReturnInternalServerError()
        {
            listConsultPortMock.Setup(
                mock => mock.ListConsult(It.IsAny<int>(), It.IsAny<int>())
            ).Throws<Exception>();

            var result = consultController.ListConsult(0, 10) as ObjectResult;

            result.StatusCode.Should().Be((int)HttpStatusCode.InternalServerError);
        }
    }
}
