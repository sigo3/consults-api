using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

using Arch.EntityFrameworkCore.UnitOfWork;

using AutoMapper;

using Domain.Consult.Adapters;
using Domain.Consult.Ports;

using Infrastructure.Common.Contexts;
using Infrastructure.Consult.Entities.Adapters;

using Refit;

using UseCase.Consult;
using Infrastructure.Standard.Adapters;

namespace WebApi
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureSettings(services);
            ConfigureSwagger(services);
            ConfigureDatabase(services);
            ConfigureHttpClient(services);
            ConfigureAutoMapper(services);
            ConfigurePorts(services);
            ConfigureAdapters(services);

            services.AddLogging(configure =>
            {
                configure.AddConsole();
            });

            services.AddCors();
            services.AddControllers();
        }

        private void ConfigureSettings(IServiceCollection services)
        {
            //services.Configure<MySetting>(configuration.GetSection(nameof(MySetting)));
        }

        private void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "ConsultApi",
                    Version = "v1"
                });
            });
        }

        private void ConfigureDatabase(IServiceCollection services)
        {
            string connectionString = configuration.GetConnectionString("MySqlContext");

            services.AddDbContext<MySqlContext>(options => options.UseMySql(connectionString));
            services.AddUnitOfWork<MySqlContext>();
        }

        private void ConfigureHttpClient(IServiceCollection services)
        {
            var standardApiBaseUrl = configuration.GetSection("RestApis:Standard:BaseUrl").Value;

            services
                .AddRefitClient<IFindStandardRestAdapter>()
                .ConfigureHttpClient(c => c.BaseAddress = new Uri(standardApiBaseUrl));

            services
                .AddRefitClient<IListStandardRestAdapter>()
                .ConfigureHttpClient(c => c.BaseAddress = new Uri(standardApiBaseUrl));
        }

        private void ConfigureAutoMapper(IServiceCollection services)
        {
            IEnumerable<Type> profiles = Assembly.Load("Infrastructure").GetTypes()
                .Where(type => typeof(Profile).IsAssignableFrom(type));

            services.AddAutoMapper(profiles.ToArray());
        }

        private void ConfigurePorts(IServiceCollection services)
        {
            services.AddScoped<ICreateConsultPort, CreateConsultUseCase>();
            services.AddScoped<IDeleteConsultPort, DeleteConsultUseCase>();
            services.AddScoped<IFindConsultByCodePort, FindConsultUseCase>();
            services.AddScoped<IListConsultPort, ListConsultUseCase>();
            services.AddScoped<IUpdateConsultPort, UpdateConsultUseCase>();
        }

        private void ConfigureAdapters(IServiceCollection services)
        {
            services.AddScoped<ICreateConsultAdapter, MySqlAdapter>();
            services.AddScoped<IDeleteConsultAdapter, MySqlAdapter>();
            services.AddScoped<IFindConsultAdapter, MySqlAdapter>();
            services.AddScoped<IListConsultAdapter, MySqlAdapter>();
            services.AddScoped<IUpdateConsultAdapter, MySqlAdapter>();

            services.AddScoped<Domain.Standard.Adapters.IFindStandardAdapter, RestAdapter>();
            services.AddScoped<Domain.Standard.Adapters.IListStandardAdapter, RestAdapter>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseCors(option => {
                option.AllowAnyOrigin();
                option.AllowAnyMethod();
                option.AllowAnyHeader();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ConsultsApi");
            });
        }
    }
}
