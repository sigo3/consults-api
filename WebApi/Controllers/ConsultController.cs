﻿using System;

using Microsoft.AspNetCore.Mvc;

using Microsoft.Extensions.Logging;

using Domain.Consult.Exceptions;
using Domain.Consult.Models;
using Domain.Consult.Ports;


namespace WebApi.Controllers
{
    [Route("consult")]
    [ApiController]
    public class ConsultController : ControllerBase
    {
        private readonly ILogger<ConsultController> logger;
        private readonly ICreateConsultPort createConsultPort;
        private readonly IDeleteConsultPort deleteConsultPort;
        private readonly IFindConsultByCodePort findConsultByCodePort;
        private readonly IListConsultPort listConsultPort;
        private readonly IUpdateConsultPort updateConsultPort;

        public ConsultController(
            ILogger<ConsultController> logger,
            ICreateConsultPort createConsultPort,
            IDeleteConsultPort deleteConsultPort,
            IFindConsultByCodePort findConsultByCodePort,
            IListConsultPort listConsultPort,
            IUpdateConsultPort updateConsultPort
        )
        {
            this.logger = logger;
            this.createConsultPort = createConsultPort;
            this.deleteConsultPort = deleteConsultPort;
            this.findConsultByCodePort = findConsultByCodePort;
            this.listConsultPort = listConsultPort;
            this.updateConsultPort = updateConsultPort;
        }

        [HttpPost]
        public IActionResult CreateConsult([FromBody] Consult consult)
        {
            try
            {
                this.createConsultPort.CreateConsult(consult);
                return Created(nameof(CreateConsult), consult);
            }
            catch (ConsultAlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut("{code}")]
        public IActionResult UpdateConsult([FromRoute] string code, [FromBody] Consult consult)
        {
            try
            {
                this.updateConsultPort.UpdateConsult(code, consult);
                return Ok();
            }
            catch (ConsultNotFoundException ex)
            {
                return NotFound(new
                {
                    Message = ex.Message
                });
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("{code}")]
        public IActionResult FindConsultByCode([FromRoute] string code)
        {
            try
            {
                var consult = this.findConsultByCodePort.FindConsultByCode(code);
                return Ok(consult);
            }
            catch (ConsultNotFoundException ex)
            {
                return NotFound(new
                {
                    Message = ex.Message
                });
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public IActionResult ListConsult([FromQuery] int page, int pageSize)
        {
            try
            {
                var consults = this.listConsultPort.ListConsult(page, pageSize);
                return Ok(consults);
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{code}")]
        public IActionResult DeleteConsult([FromRoute] string code)
        {
            try
            {
                this.deleteConsultPort.DeleteConsult(code);
                return NoContent();
            }
            catch (Exception ex)
            {
                this.logger.LogError(ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
